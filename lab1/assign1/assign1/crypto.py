#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this with a description of the program.
"""
from math import ceil
import utils

# Caesar Cipher

def encrypt_caesar(plaintext):
    result=''
    for i in plaintext:
        #hanyadik betu az abc-ben + ne fussunk ki az abc-bol
        result += chr((ord(i) + 3-65) % 26 + 65)
    print(result)

def decrypt_caesar(ciphertext):
    ABC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    letters=[]
    for key in ABC:
        letters.append(key)
    result=''
    for i in ciphertext:
        k=0
        for j in letters:
            if i == j:
                result+=letters[k-3]
            k+=1
    print(result)

# Vigenere Cipher


def encrypt_vigenere(plaintext, keyword):
    keyword = list(keyword)
    if len(plaintext) != len(keyword):
        k=len(plaintext)-len(keyword)
        kk=len(keyword)
        for i in range(k):
            keyword.append(keyword[i % kk])
    result=''
    for j in range(len(plaintext)):
        result += chr((ord(plaintext[j]) + (ord(keyword[j])-65) - 65) % 26 + 65)
    print(result)

def decrypt_vigenere(ciphertext, keyword):
    keyword = list(keyword)
    if len(ciphertext) != len(keyword):
        k=len(ciphertext)-len(keyword)
        kk=len(keyword)
        for i in range(k):
            keyword.append(keyword[i % kk])
    print(keyword)
    ABC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    letters=[]
    for key in ABC:
        letters.append(key)
    result=''
    m=0
    for i in ciphertext:
        k=0
        for j in letters:
            if i == j:
                o=(ord(keyword[m])-65)
                result+=letters[k-o]
            k+=1
        m+=1
    print(result)


def encrypt_scytale(plaintext, circumference):
    plain = []
    for i in plaintext:
        plain.append(i)
    result=''
    for j in range(circumference):
        k=j
        while k < len(plaintext):
            result+=plaintext[k]
            k+=circumference
    print(result)

def decrypt_scytale(ciphertext, circumference):
    m = ceil(len(ciphertext) / circumference)
    matrix = [[ "0" for x in range(m)] for y in range(circumference)]
    s=0
    o=0
    i=0
    while i < len(ciphertext):
        while o < m and i < len(ciphertext):
            matrix[s][o] = ciphertext[i]
            o+=1
            i+=1
        o=0
        s+=1
    result=''
    for j in range(m):
        for i in range(circumference):
            if (matrix[i][j] != "0"):
                result+=matrix[i][j]
    print(result)

def encrypt_railfence(plaintext, num_rails):
    rail = [[ "0" for x in range(len(plaintext))] for y in range(num_rails)]
    le = 1
    s=0
    o=0
     
    for i in range(len(plaintext)):
        if (s == 0) or (s == num_rails - 1 ):
            if le:
                le=0
            else:
                le=1 
        rail[s][o] = plaintext[i]
        o += 1
        if le:
            s-=1
        else:
            s+=1
    result=''
    for i in range(num_rails):
        for j in range(len(plaintext)):
            if (rail[i][j] != "0"):
                result+=rail[i][j]
    print(result)

def decrypt_railfence(ciphertext, num_rails):
    rail = [[ "0" for x in range(len(ciphertext))] for y in range(num_rails)]
    le = 1
    s=0
    o=0
     
    for i in range(len(ciphertext)):
        if s == 0:
            le = 0
        elif s == num_rails - 1:
            le = 1
        rail[s][o] = '$'
        o += 1
        if le:
            s -= 1
        else:
            s += 1
             
    k = 0
    for i in range(num_rails):
        for j in range(len(ciphertext)):
            if (rail[i][j] == '$'):
               if (k < len(ciphertext)):
                rail[i][j] = ciphertext[k]
                k += 1
         
    result = ''
    s, o = 0, 0
    for i in range(len(ciphertext)):
        if s == 0:
            le = 0
        elif s == num_rails-1:
            le = 1  
        if (rail[s][o] != '$'):
            result+=rail[s][o]
            o += 1
        if le:
            s -= 1
        else:
            s += 1
    print(result)

# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here

