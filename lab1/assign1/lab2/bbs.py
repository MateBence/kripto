def blum_blum_shub(h, seed):
    p = 37
    q = 31

    n = p*q
    z = ''
    x  = (seed * seed) % n
    for i in range(h):
        x = (x << 1)
        x = (x*x) % n 
        p=x%2 
        z += str(p)
    
    return z
