def encrypt(f, message, par):
    ciphertext = ''
    for (m, k) in zip(message, f(len(message), par)):
        ciphertext += chr(ord(m) ^ ord(k))
    return ciphertext


def decrypt(f, message, par):
    return encrypt(f, message, par)