import socket
import threading
import bbs as s
import cipher as c

client_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

host = "127.0.0.1"
port = 5000

szerver = (host,port)
client_socket.connect(szerver)

seed = 48816701

def recieve():
    while True:
        try:
            key = client_socket.recv(1024).decode()
            message = client_socket.recv(1024).decode()
           
            msg = c.decrypt(s.blum_blum_shub, message, seed)
            print('Recieved:', msg)
        except:
            client_socket.close()
            break


def send_message():
    read = threading.Thread(target=recieve)
    read.start()
    while True:
        message = input('Write something:')
        key = s.blum_blum_shub(len(message), seed)
        msg = c.encrypt(s.blum_blum_shub, message, seed)


        client_socket.send(key.encode())
        client_socket.send(msg.encode())


write_to_server = threading.Thread(target=send_message)
write_to_server.start()

