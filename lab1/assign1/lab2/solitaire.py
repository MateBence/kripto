def solitaire(n, pakli):
    key = ''
    pakli_size = len(pakli)

    while len(key) < n:
        # a)
        white_joker = pakli.index(53)
        
        if white_joker != pakli_size - 1:
            # kicsereljuk az alatta levo kartyalappal
            pakli[white_joker], pakli[white_joker+1] = pakli[white_joker+1], pakli[white_joker]
            white_joker += 1
        else:
            # beszurjuk felulrol az elso kartyalap utan
            pakli[1],  pakli[2:] = pakli[white_joker], pakli[1:white_joker]
            white_joker = 1

        # b)
        black_joker = pakli.index(54)

        if black_joker == pakli_size - 2:
            # a legfelso kartyalap ala tesszuk
            pakli[1], pakli[2:] = pakli[black_joker], pakli[1:black_joker] + pakli[black_joker:]
            black_joker = 1

        elif  black_joker == pakli_size - 1:
            # felulrol a masodik kartyalap utan szurjuk
            pakli[2], pakli[3:] = pakli[black_joker], pakli[2:black_joker]
            black_joker = 2
        else:
            # ket kartyalappal lennebb szurjuk
            pakli[black_joker + 2], pakli[black_joker:black_joker+2] = pakli[black_joker], pakli[black_joker+1:black_joker+3]
            black_joker += 2

        # c)
        jk1 = min(white_joker, black_joker)
        jk2 = max(white_joker, black_joker)

        # elso joker elotti kartyalap felcserelese a masodik joker utani kartyalapokkal
        pakli = pakli[jk2 + 1:] + pakli[jk1:jk2 + 1] + pakli[:jk1]

        # d)

        count = min(pakli[-1], 53)
        # felülrol az elso n kártyát kicseréljük a következo 53−n kártyával
        pakli[:-1] = pakli[count:-1] + pakli[:count]


        # e)
        if pakli[0] != pakli[black_joker] and pakli[0] != pakli[white_joker]:
            key += str(pakli[pakli[0]])

    return key